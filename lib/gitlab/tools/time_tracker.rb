require 'gitlab/tools/helpers'
require 'gitlab/tools/api'

module Gitlab
  module Tools
    class TimeTracker

      include Helpers

      def initialize(options)
        @options = {
          output: 'pdf'
        }.merge(options)

        @api = Api.new(@options)
      end

      def set_time_spent
        @api.issues.each do |issue|
          puts "##{issue.id} #{issue.title} #{issue.weight} #{issue.labels}"
          duration, merge_time = @api.get_issue_duration(issue)
          puts "Spent : #{seconds_to_s(duration)} / Merge #{seconds_to_s(merge_time)}"
        end
      end
    end
  end
end
