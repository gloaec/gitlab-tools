require 'gitlab/tools/helpers'
require 'gitlab/tools/api'

module Gitlab
  module Tools
    class Mover

      include Helpers

      def initialize(options, args)
        @options = {
          group: '',
          force: false
        }.merge(options)

        @sourceId, @destinationId = args

        @api = Api.new(@options)
      end

      def move
        group = @api.group
        raise "Invalid destination" unless @destinationId.start_with?('&')
        destination = @api.get_epic(@destinationId[1..-1])

        if @sourceId.start_with?('&')
          source = @api.get_epic(@sourceId[1..-1])
          puts "Moving".blue + " #{source.title.light_blue} to #{destination.title.light_blue}"
          move_epic(source, destination, @options[:force])
        end
      end

      def move_issue(source, destination)
      end

      def move_epic(source, destination, delete_source=false)
        puts ['Creating new epic'.blue, source.title.light_blue].join(' ')
        epic = @api.create_epic({
          **source.to_h,
          parent_id: destination.id
        })
        page = 1
        issues = []
        loop do
          puts ['Getting issues'.blue, 'from', source.title.light_blue, "(page #{page.to_s.blue})"].join(' ')
          _issues = @api.epic_issues(source.iid, { page: page, per_page: 100 })
          issues += _issues
          page += 1
          break if _issues.length < 100
        end
        puts "=> #{issues.length.to_s.blue} issues"
        issues.each do |issue|
          puts ['Moving issue'.blue, issue.title.light_blue, 'to', epic.title.light_blue].join(' ')
          puts issue.project_id, issue.iid, issue.epic_iid, epic.iid, epic.id
          @api.edit_issue(issue.project_id, issue.iid, { epic_iid: epic.iid })
        end
        @api.create_epic_note(source.id, "Moved to &#{epic.iid}")
        @api.edit_epic(source.iid, {
          title: "[MOVED] #{source.title}",
          state_event: 'close'
        })
        @api.delete_epic(source.id) if delete_source
      end
    end
  end
end
