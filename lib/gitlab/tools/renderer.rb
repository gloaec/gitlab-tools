require 'gitlab/tools/helpers'
require 'active_support/core_ext/string/output_safety'
require 'filemagic'

module Gitlab
  module Tools
    module Renderer
      include Helpers

      ENCODING = 'UTF-8'
      ASSET_URL_REGEX = /url\(['"]?([^'"]+?)['"]?\)/

      def get_template(path)
        File.read(File.join(__dir__, 'templates', path), encoding: ENCODING)
      end

      def render(template, options = {})
        layout = options.delete(:layout)
        if layout
          render(layout) do
            render(template, options)
          end
        else
          template_file = get_template(template)
          b = binding
          options.map { |k, v|  b.local_variable_set(k, v.is_a?(Hash) ? to_object(v) : v) }
          ERB.new(template_file, nil, '-').result(b)
        end
      end

      def assets_path
        File.join(__dir__, 'assets')
      end

      def asset_base64(path)
        asset = find_asset(path)
        raise "Could not find asset '#{path}'" if asset.nil?
        content_type = MIME::Types.type_for(asset.path).first.content_type
        base64 = Base64.encode64(asset.read).gsub(/\s+/, '')
        "data:#{content_type};base64,#{base64}"
      end

      def stylesheet_link_tag(*sources)
        stylesheet_contents = sources.collect do |source|
          source = add_extension(source, 'css')
          "<style type='text/css'>#{read_asset(source)}</style>"
        end.join("\n")

        stylesheet_contents.gsub(ASSET_URL_REGEX) do
          if Regexp.last_match[1].starts_with?('data:')
            "url(#{Regexp.last_match[1]})"
          else
            "url(#{asset_path(Regexp.last_match[1])})"
          end
        end.html_safe
      end

      def stylesheet_pack_tag(*sources)
        return unless defined?(Webpacker)
        if running_in_development?
          stylesheet_pack_tag(*sources)
        else
          css_text = sources.collect do |source|
            source = add_extension(source, 'css')
            stylesheet_link_tag(webpacker_source_url(source))
          end.join("\n")
          css_text.respond_to?(:html_safe) ? css_text.html_safe : css_text
        end
      end

      def javascript_pack_tag(*sources)
        return unless defined?(Webpacker)

        if running_in_development?
          javascript_pack_tag(*sources)
        else
          sources.collect do |source|
            source = add_extension(source, 'js')
            "<script type='text/javascript'>#{read_asset(webpacker_source_url(source))}</script>"
          end.join("\n").html_safe
        end
      end

      def image_tag(img, options = {})
        image_tag asset_path(img), options
      end

      def javascript_src_tag(jsfile, options = {})
        jsfile = add_extension(jsfile, 'js')
        javascript_include_tag asset_path(jsfile), options
      end

      def javascript_include_tag(*sources)
        sources.collect do |source|
          source = add_extension(source, 'js')
          "<script type='text/javascript'>#{read_asset(source)}</script>"
        end.join("\n").html_safe
      end

      def asset_path(asset)
        if (pathname = asset_pathname(asset).to_s) =~ URI_REGEXP
          pathname
        else
          "file:///#{pathname}"
        end
      end

      def asset_pack_path(asset)
        return unless defined?(Webpacker)

        if running_in_development?
          asset_pack_path(asset)
        else
          asset_path webpacker_source_url(asset)
        end
      end

      private

      # borrowed from actionpack/lib/action_view/helpers/asset_url_helper.rb
      URI_REGEXP = %r{^[-a-z]+://|^(?:cid|data):|^//}

      def asset_pathname(source)
        if precompiled_or_absolute_asset?(source)
          #asset = asset_path(source)
          #pathname = prepend_protocol(asset)
          #if pathname =~ URI_REGEXP
          #  # asset_path returns an absolute URL using asset_host if asset_host is set
          #  pathname
          #else
          #  File.join(assets_path, asset.sub(/\A#{"[REPLACEME]"}/, ''))
          #end
        else
          asset = find_asset(source)
          if asset
            # older versions need pathname, Sprockets 4 supports only filename
            asset.respond_to?(:path) ? asset.path : asset
          else
           File.join(assets_path, source)
          end
        end
      end

      def find_asset(path)
        File.open(File.join(assets_path, path))
      end

      # will prepend a http or default_protocol to a protocol relative URL
      # or when no protcol is set.
      def prepend_protocol(source)
        protocol = WickedPdf.config[:default_protocol] || 'http'
        if source[0, 2] == '//'
          source = [protocol, ':', source].join
        elsif source[0] != '/' && !source[0, 8].include?('://')
          source = [protocol, '://', source].join
        end
        source
      end

      def precompiled_or_absolute_asset?(source)
        source.to_s[0] == '/' ||
        source.to_s.match(/\Ahttps?\:\/\//)
      end

      def read_asset(source)
        if precompiled_or_absolute_asset?(source)
          pathname = asset_pathname(source)
          if pathname =~ URI_REGEXP
            read_from_uri(pathname)
          elsif File.file?(pathname)
            IO.read(pathname)
          end
        else
          find_asset(source).to_s
        end
      end

      def read_from_uri(uri)
        asset = Net::HTTP.get(URI(uri))
        asset.force_encoding('UTF-8') if asset
        asset = gzip(asset) if WickedPdf.config[:expect_gzipped_remote_assets]
        asset
      end

      def gzip(asset)
        stringified_asset = StringIO.new(asset)
        gzipper = Zlib::GzipReader.new(stringified_asset)
        gzipper.read
      rescue Zlib::GzipFile::Error
        nil
      end

      def webpacker_source_url(source)
        return unless defined?(Webpacker) && defined?(Webpacker::VERSION)
        # In Webpacker 3.2.0 asset_pack_url is introduced
        if Webpacker::VERSION >= '3.2.0'
          asset_pack_url(source)
        else
          source_path = asset_pack_path(source)
          # Remove last slash from root path
          root_url[0...-1] + source_path
        end
      end

      def running_in_development?
        return unless defined?(Webpacker)
        # :dev_server method was added in webpacker 3.0.0
        if Webpacker.respond_to?(:dev_server)
          Webpacker.dev_server.running?
        else
          false
        end
      end
    end
  end
end

