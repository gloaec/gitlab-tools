# frozen_string_literal: true

class Gitlab::Client
  # Defines methods related to iterations.
  # @see https://docs.gitlab.com/ce/api/iterations.html
  module Iterations
    # Gets a list of project's iterations.
    #
    # @example
    #   Gitlab.iterations(5)
    #
    # @param  [Integer, String] project The ID or name of a project.
    # @param  [Hash] options A customizable set of options.
    # @option options [Integer] :page The page number.
    # @option options [Integer] :per_page The number of results per page.
    # @return [Array<Gitlab::ObjectifiedHash>]
    def iterations(project, options = {})
      get("/projects/#{url_encode project}/iterations", query: options)
    end

    # Gets a single iteration.
    #
    # @example
    #   Gitlab.iteration(5, 36)
    #
    # @param  [Integer, String] project The ID or name of a project.
    # @param  [Integer] id The ID of a iteration.
    # @return [Gitlab::ObjectifiedHash]
    def iteration(project, id)
      get("/projects/#{url_encode project}/iterations/#{id}")
    end

    # Gets the issues of a given iteration.
    #
    # @example
    #   Gitlab.iteration_issues(5, 2)
    #
    # @param  [Integer, String] project The ID or name of a project.
    # @param  [Integer, String] iteration The ID of a iteration.
    # @option options [Integer] :page The page number.
    # @option options [Integer] :per_page The number of results per page.
    # @return [Array<Gitlab::ObjectifiedHash>]
    def iteration_issues(project, iteration, options = {})
      get("/projects/#{url_encode project}/iterations/#{iteration}/issues", query: options)
    end

    # Gets the merge_requests of a given iteration.
    #
    # @example
    #   Gitlab.iteration_merge_requests(5, 2)
    #
    # @param  [Integer, String] project The ID or name of a project.
    # @param  [Integer, String] iteration The ID of a iteration.
    # @option options [Integer] :page The page number.
    # @option options [Integer] :per_page The number of results per page.
    # @return [Array<Gitlab::ObjectifiedHash>]
    def iteration_merge_requests(project, iteration, options = {})
      get("/projects/#{url_encode project}/iterations/#{iteration}/merge_requests", query: options)
    end

    # Creates a new iteration.
    #
    # @example
    #   Gitlab.create_iteration(5, 'v1.0')
    #
    # @param  [Integer, String] project The ID or name of a project.
    # @param  [String] title The title of a iteration.
    # @param  [Hash] options A customizable set of options.
    # @option options [String] :description The description of a iteration.
    # @option options [String] :due_date The due date of a iteration.
    # @return [Gitlab::ObjectifiedHash] Information about created iteration.
    def create_iteration(project, title, options = {})
      body = { title: title }.merge(options)
      post("/projects/#{url_encode project}/iterations", body: body)
    end

    # Updates a iteration.
    #
    # @example
    #   Gitlab.edit_iteration(5, 2, { state_event: 'activate' })
    #
    # @param  [Integer, String] project The ID or name of a project.
    # @param  [Integer] id The ID of a iteration.
    # @param  [Hash] options A customizable set of options.
    # @option options [String] :title The title of a iteration.
    # @option options [String] :description The description of a iteration.
    # @option options [String] :due_date The due date of a iteration.
    # @option options [String] :state_event The state of a iteration ('close' or 'activate').
    # @return [Gitlab::ObjectifiedHash] Information about updated iteration.
    def edit_iteration(project, id, options = {})
      put("/projects/#{url_encode project}/iterations/#{id}", body: options)
    end

    # Delete a project iteration.
    #
    # @example
    #   Gitlab.delete_iteration(5, 2)
    #
    # @param  [Integer, String] project The ID or name of a project.
    # @param  [Integer] id The ID of a iteration.
    # @return [nil] This API call returns an empty response body.
    def delete_iteration(project, id)
      delete("/projects/#{url_encode project}/iterations/#{id}")
    end
  end
end
