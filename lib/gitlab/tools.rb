require "gitlab/tools/version"
require "gitlab/tools/report_builder"
require "gitlab/tools/changelog_builder"
require "gitlab/tools/time_tracker"
require "gitlab/tools/mover"

module Gitlab
  module Tools
    class Error < StandardError; end

    def self.call(args, options = {})

      command = args.shift

      case command
      when 'generate', 'g'
        builder = Gitlab::Tools::ReportBuilder.new(options)
        builder.generate
      when 'changelog', 'c'
        builder = Gitlab::Tools::ChangelogBuilder.new(options)
        builder.generate
      when 'timestamp', 't'
        tracker = Gitlab::Tools::TimeTracker.new(options)
        tracker.set_time_spent
      when 'move', 'mv'
        mover = Gitlab::Tools::Mover.new(options, args)
        mover.move
      end

    end

    def self.root
      Gem::Specification.find_by_name('gitlab-tools').gem_dir
    end
  end
end
