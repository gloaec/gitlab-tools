# frozen_string_literal: true

class Gitlab::Client
  # Defines methods related to group iterations.
  # @see https://docs.gitlab.com/ee/api/group_iterations.html
  module GroupIterations
    # Gets a list of a group's iterations.
    #
    # @example
    #   Gitlab.group_iterations(5)
    #
    # @param  [Integer, String] id The ID or name of a group.
    # @param  [Hash] options A customizable set of options.
    # @option options [Integer] :page The page number.
    # @option options [Integer] :per_page The number of results per page.
    # @return [Array<Gitlab::ObjectifiedHash>]
    def group_iterations(id, options = {})
      get("/groups/#{url_encode id}/iterations", query: options)
    end

    # Gets a single group iteration.
    #
    # @example
    #   Gitlab.group_iteration(5, 36)
    #
    # @param  [Integer, String] id The ID or name of a group.
    # @param  [Integer] iteration_id The ID of a iteration.
    # @return [Gitlab::ObjectifiedHash]
    def group_iteration(id, iteration_id)
      get("/groups/#{url_encode id}/iterations/#{iteration_id}")
    end

    # Creates a new group iteration.
    #
    # @example
    #   Gitlab.create_group_iteration(5, 'v1.0')
    #
    # @param  [Integer, String] id The ID or name of a group.
    # @param  [String] title The title of a iteration.
    # @param  [Hash] options A customizable set of options.
    # @option options [String] :description The description of a iteration.
    # @option options [String] :due_date The due date of a iteration.
    # @return [Gitlab::ObjectifiedHash] Information about created iteration.
    def create_group_iteration(id, title, options = {})
      body = { title: title }.merge(options)
      post("/groups/#{url_encode id}/iterations", body: body)
    end

    # Updates a group iteration.
    #
    # @example
    #   Gitlab.edit_group_iteration(5, 2, { state_event: 'activate' })
    #
    # @param  [Integer, String] id The ID or name of a group.
    # @param  [Integer] iteration_id The ID of a iteration.
    # @param  [Hash] options A customizable set of options.
    # @option options [String] :title The title of a iteration.
    # @option options [String] :description The description of a iteration.
    # @option options [String] :due_date The due date of a iteration.
    # @option options [String] :state_event The state of a iteration ('close' or 'activate').
    # @return [Gitlab::ObjectifiedHash] Information about updated iteration.
    def edit_group_iteration(id, iteration_id, options = {})
      put("/groups/#{url_encode id}/iterations/#{iteration_id}", body: options)
    end

    # Gets the issues of a given group iteration.
    #
    # @example
    #   Gitlab.group_iteration_issues(5, 2)
    #
    # @param  [Integer, String] id The ID or name of a group.
    # @param  [Integer, String] iteration_id The ID of a iteration.
    # @option options [Integer] :page The page number.
    # @option options [Integer] :per_page The number of results per page.
    # @return [Array<Gitlab::ObjectifiedHash>]
    def group_iteration_issues(id, iteration_id, options = {})
      get("/groups/#{url_encode id}/iterations/#{iteration_id}/issues", query: options)
    end

    # Gets the merge_requests of a given group iteration.
    #
    # @example
    #   Gitlab.group_iteration_merge_requests(5, 2)
    #
    # @param  [Integer, String] group The ID or name of a group.
    # @param  [Integer, String] iteration_id The ID of a iteration.
    # @option options [Integer] :page The page number.
    # @option options [Integer] :per_page The number of results per page.
    # @return [Array<Gitlab::ObjectifiedHash>]
    def group_iteration_merge_requests(id, iteration_id, options = {})
      get("/groups/#{url_encode id}/iterations/#{iteration_id}/merge_requests", query: options)
    end
  end
end
