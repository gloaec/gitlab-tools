require_relative 'lib/gitlab/tools/version'

Gem::Specification.new do |spec|
  spec.name          = "gitlab-tools"
  spec.version       = Gitlab::Tools::VERSION
  spec.authors       = ["Ghislain Loaec"]
  spec.email         = ["ghislain.loaec@gmail.com"]
  spec.license       = 'MIT'

  spec.summary       = "Gitlab Tools"
  spec.description   = "Uses gitlab API to extract issues, MRs, commits, epics, milestones, iterations and assignees"
  spec.homepage      = 'https://rubygems.org/gems/gitlab-tools'
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/gloaec/gitlab-tools"
  spec.metadata["changelog_uri"] = "https://gitlab.com/gloaec/gitlab-tools/-/blob/develop/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'activesupport', '~> 6.1.4'
  spec.add_runtime_dependency 'colorize', '~> 0.8.1'
  spec.add_runtime_dependency 'ruby-filemagic', '~> 0.7.2'
  spec.add_runtime_dependency 'wicked_pdf', '~> 2.1.0'
  spec.add_runtime_dependency 'tilt', '~> 2.0.10'
  spec.add_runtime_dependency 'gitlab', '~> 4.17'
end
