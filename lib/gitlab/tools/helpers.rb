require 'active_support/core_ext/object/blank.rb'
require 'ostruct'
require 'colorize'

module Gitlab
  module Tools
    module Helpers

      def uri_join(*args)
        args.map { |arg| arg.gsub(%r{^/*(.*?)/*$}, '\1') }.join("/")
      end

      def to_object(hash)
        Gitlab::ObjectifiedHash.new(hash)
        #OpenStruct.new(hash.each_with_object({}) do |(key, val), memo|
        #  memo[key] = val.is_a?(Hash) ? to_object(val) : val
        #end)
      end

      def deep_clone(object)
        Marshal.load(Marshal.dump(object))
      end

      def add_extension(filename, extension)
        filename.to_s.split('.').include?(extension) ? filename : "#{filename}.#{extension}"
      end

      def work_seconds_between(started_at, ended_at)
        started_date = day = started_at.to_date
        started_time = started_at.to_time
        ended_date = ended_at.to_date
        ended_time = ended_at.to_time
        start_day_end = DateTime.new(started_date.year, started_date.month, started_date.day, 17, 00, 0).to_time
        end_day_start = DateTime.new(ended_date.year, ended_date.month, ended_date.day, 10, 00, 0).to_time
        if started_date != ended_date
          time = [start_day_end - started_time, 0].max
          #puts "#{day}: #{'%.2f' % (time/3600)}h"
          while (day = day.next_day) < ended_date
            case day.cwday
            when 6,7
              # Nothing on weekends
            else
              #puts "#{day}: 7h"
              time += 25200 # 7 hrs
            end
          end
          #puts "#{day}: #{'%.2f' % ((ended_at.to_time - end_day_start)/3600)}h"
          time += [ended_time - end_day_start, 0].max
        else
          ended_time - started_time
        end
      end

      def seconds_to_s(duration)
        [[60, :s], [60, :m], [7, :h], [5, :d], [4, :w], [Float::INFINITY, :mo]].map{ |count, name|
          if duration && duration > 0
            duration, n = duration.divmod(count)
            if name == :h && n >= 8
              duration += 1
              n = 0
            end
            "#{n.to_i}#{name}" unless n.to_i==0
          end
        }.compact.reverse.join(' ')
      end

      def state_color(state)
        {
          closed: 'danger',
          merged: 'danger',
          done: 'warning',
          draft: 'success',
          doing: 'success',
          opened: 'light',
        }[state&.to_sym]
      end
    end
  end
end
