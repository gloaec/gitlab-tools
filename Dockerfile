FROM ruby:3 AS builder
WORKDIR /app
COPY . .
RUN gem build gitlab-tools.gemspec -o gitlab-tools.gem

FROM ruby:2.7.5-alpine3.13
WORKDIR /app
RUN apk add build-base libmagic file-dev git
#RUN mkdir .gems
#ENV GEM_HOME="/app/.gems"
#ENV PATH="$PATH:$GEM_HOME/bin"
COPY --from=builder /app/gitlab-tools.gem ./
RUN gem install ./gitlab-tools.gem # --install-dir .gems
CMD ["sleep", "infinity"]
