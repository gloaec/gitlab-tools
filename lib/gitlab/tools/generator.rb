require 'gitlab'

module Gitlab
  module Tools
    class Generator
      GITLAB_API_VERSION = 4

      include ::Helpers

      def initialize(options = {})
        @options = {
          milestone: '',
          assignee: '',
          host: 'https://gitlab.com/',
          private_token: options[:token] || "",
          group: '',
          project: '',
          doing_label: 'doing'
        }.merge(options)

        Gitlab.configure do |config|
          config.endpoint       = uri_join(@options[:host], "api/v#{GITLAB_API_VERSION}/") # || ENV['GITLAB_API_ENDPOINT'] || ENV['CI_API_V4_URL']
          config.private_token  = @options[:token] # || ENV['GITLAB_API_PRIVATE_TOKEN']
        end

        @gitlab = Gitlab.client()
      end

      #def api_get(path, options = {})
      #  uri = uri_join(@options[:host], "api/v#{GITLAB_API_VERSION}/", path)
      #  puts "Requesting #{uri}..."
      #  response = HTTParty.get(uri, {
      #    **options,
      #    headers: {
      #      'Private-Token': @options[:token],
      #      'Content-Type': 'application/json'
      #    }
      #  })
      #  JSON.parse(response.body)
      #end

      def generate!
        puts "Generating report"
        puts "- Group: #{@options[:group]}" if @options[:group].present?
        puts "- Project: #{@options[:project]}" if @options[:project].present?
        puts "- Milestone: #{@options[:milestone]}" if @options[:milestone].present?
        puts "- Assignee: #{@options[:assignee]}" if @options[:assignee].present?

        set_user
        set_group
        set_project
        set_assignee
        get_issues

        @issues.each do |issue|
          puts "##{issue.id} #{issue.title} #{issue.weight} #{issue.labels}"
          duration, merge_time = get_issue_duration(issue)
          puts "Spent : #{seconds_to_s(duration)} / Merge #{seconds_to_s(merge_time)}"
        end

        nil
      end

      private

      def set_user
        @user = @gitlab.user
        puts "Connected as #{@user.name}"
      end

      def set_group
        options = {
          **@options.slice(:private_token),
        }
        if @options[:group_id].present?
          puts "Looking for group_id '#{@options[:group_id]}'..."
          @group = @gitlab.group(@options[:group_id], options)
          puts "> Found: #{@group.name} (ID: #{@group.id})"
        elsif @options[:group].present?
          puts "Looking for group '#{@options[:group]}'..."
          @group = @gitlab.group_search(@options[:group], per_page: 1, **options)&.first
          @options[:group_id] = @group.id
          puts "> Found: #{@group.name} (ID: #{@group.id})"
        end
        @group
      end

      def set_project
        if @options[:project_id].present?
          puts "Looking for project_id '#{@options[:project_id]}'..."
          @project = @gitlab.project(@options[:project_id])
          puts "> Found: #{@project.name} (ID: #{@project.id})"
        elsif @options[:project].present?
          puts "Looking for project '#{@options[:project]}'..."
          @project = @gitlab.project_search(@options[:project], per_page: 1)
          @options[:project_id] = @project.id
          puts "> Found: #{@project.name} (ID: #{@project.id})"
        end
        @project
      end

      def set_assignee
        if @options[:assignee_id].present?
          puts "Looking for assignee_id '#{@options[:assignee_id]}'..."
          @assignee = @gitlab.user(@options[:assignee_id])
          puts "> Found: #{@assignee.name} (ID: #{@assignee.id})"
        elsif @options[:assignee].present?
          puts "Looking for assignee '#{@options[:assignee]}'..."
          @assignee = @gitlab.user_search(@options[:assignee], per_page: 1)&.first
          @options[:assignee_id] = @assignee.id
          puts "> Found: #{@assignee.name} (ID: #{@assignee.id})"
        end
        @assignee
      end

      def get_issues
        options = {
          **@options.slice(:milestone, :assignee_id).reject { |k, v| v.blank? },
          per_page: 100
        }
        if @project then
          puts "Looking for issues for project '#{@project.name}' (#{options.to_json})..."
          @issues = @gitlab.issues(@project.id, options)
          puts "> Found #{@issues.count} issues"
        elsif @group
          puts "Looking for issues for group '#{@group.name}' (#{options.to_json})..."
          @issues = @gitlab.group_issues(@group.id, options)
          puts "> Found #{@issues.count} issues"
        else
          puts "Looking for issues (#{options.to_json})..."
          @issues = @gitlab.issues(nil, options)
          puts "> Found #{@issues.count} issues"
        end
        @issues
      end

      def get_issue_duration(issue)
        started_at = nil
        ended_at = nil
        duration = 0
        events = get_issue_label_events(issue.project_id, issue.iid)
        events.sort { |a, b| a.created_at <=> b.created_at }.each do |event|
          if event.label&.name == @options[:doing_label]
            if event.action == 'add'
              started_at = Time.parse(event.created_at)
            elsif event.action == 'remove' && started_at
              ended_at = Time.parse(event.created_at)
              duration += ended_at - started_at
              started_at = nil
            end
          end
        end
        if issue.closed_at
          closed_at = Time.parse(issue.closed_at)
          merge_time = closed_at - (ended_at || closed_at)
        end
        return duration, merge_time
      end

      def get_issue_label_events(project, issue_iid)
        @gitlab.issue_label_events(project, issue_iid)
      end

    end
  end
end
