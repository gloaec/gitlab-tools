
# frozen_string_literal: true

class Gitlab::Client
  # Defines methods related to iterations.
  # @see https://docs.gitlab.com/ce/api/iterations.html
  module MergeRequestsRelatedToIssue
    def merge_requests_related_to_issue(issue, options = {})
      get("/projects/#{issue.project_id}/issues/#{issue.iid}/related_merge_requests", query: options)
    end
  end
end
