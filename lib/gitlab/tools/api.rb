require 'object'
require 'gitlab'
require 'gitlab/tools/helpers'
require 'fileutils'
require 'base64'

# Patch Gitlab gem for iterations
Gitlab::Client.module_eval do
  Dir[File.expand_path('../client/*.rb', __dir__)].each { |f| require f }
  include Gitlab::Client::GroupIterations
  include Gitlab::Client::Iterations
  include Gitlab::Client::ResourceIterationEvents
  include Gitlab::Client::MergeRequestsRelatedToIssue
end

module Gitlab
  module Tools
    class Api
      GITLAB_API_VERSION = 4
      MAX_THREADS = 10

      include Helpers

      def initialize(options = {})
        @options = {
          milestone: '',
          iteration: '',
          assignee: '',
          host: 'https://gitlab.com/',
          private_token: options[:token] || "",
          group: '',
          project: '',
          doing_label: 'state::doing',
          done_label: 'state::review',
          enhancement_label: 'type::enhancement',
          bug_label: 'type::bug',
          ref_name: nil,
          cache: false,
        }.merge(options)

        token_file = File.join(__dir__, '../../../.token')

        @options[:token] ||= ENV['GITLAB_TOKEN']
        @options[:token] ||= File.read(token_file) if File.exists?(token_file)

        Gitlab.configure do |config|
          config.endpoint       = uri_join(@options[:host], "api/v#{GITLAB_API_VERSION}/") # || ENV['GITLAB_API_ENDPOINT'] || ENV['CI_API_V4_URL']
          config.private_token  = @options[:token] # || ENV['GITLAB_API_PRIVATE_TOKEN']
        end

        FileUtils.mkdir_p '../../../cache' if @options[:cache]

        puts "Connected as ".blue + user.name.light_blue
      end

      def scope
        {
          milestone: @options[:milestone],
          iteration_id: iteration&.id,
          iteration_at: nil,
          assignee_id: assignee&.id,
          project_id: project&.id,
          group_id: group&.id,
          updated_after: @options[:updated_after],
          updated_before: @options[:updated_before]
        }.reject { |k, v| v.blank? }
      end

      def gitlab
        @gitlab ||= Gitlab.client
      end

      def user
        @user ||= get_user
      end

      def issues
        @issues ||= get_cached_issues if @options[:cache]
        if iterations.any?
          @issues ||= iterations.map do |i|
            get_issues(iteration_id: i.id)
          end.flatten
        else
          @issues ||= get_issues
        end
      end

      def merge_requests
        @merge_requests ||= get_cached_merge_requests if @options[:cache]
        @merge_requests ||= get_merge_requests.map {|merge_request|
          merge_request
        }
      end

      def commits
        @commits ||= get_cached_commits if @options[:cache]
        @commits ||= get_commits
      end

      def tags
        @tags ||= get_cached_tags if @options[:cache]
        @tags ||= get_tags
      end

      def changelog
        @changelog ||= get_changelog
      end

      def group
        @group ||= get_cached_group if @options[:cache]
        @group ||= get_group
      end

      def groups
        @groups ||= get_cached_groups if @options[:cache]
        @groups ||= get_groups
      end

      def project
        @project ||= get_cached_project if @options[:cache]
        @project ||= get_project
      end

      def projects
        @projects ||= get_cached_projects if @options[:cache]
        @projects ||= get_projects
      end

      def assignee
        @assignee = defined?(@assignee) ? @assignee : get_assignee
      end

      def milestone
        @milestone ||= get_milestone
      end

      def iteration
        @iteration ||= get_iteration
      end

      def iterations
        @iterations ||= get_iterations
      end

      def members
        @members ||= get_members
      end

      def get_issues(options = {}, log = true)
        issues = []

        options = {
          **scope,
          per_page: 100,
          **options
        }
        state_order = %w(closed done doing opened)

        print "Load issues ".blue, "(", scope.to_json.italic.light_blue, ") : " if log

        page = 1
        loop do
          if project then
            next_issues = gitlab.issues(project.id, **options, page:)
          elsif group
            next_issues = gitlab.group_issues(group.id, **options, page:)
          else
            next_issues = gitlab.issues(nil, **options, page:)
          end
          break if next_issues.empty?
          print "."
          issues += next_issues
          page += 1
        end

        puts " #{issues.count} issues".send(issues.count > 0 ? :green : :red) if log

        issues.each_slice(MAX_THREADS).with_index do |(*group), i|
          group.map { |issue|
            Thread.new { decorate_issue(issue) }
          }.map(&:value).each.with_index { |issue, j|
            issues[i*MAX_THREADS+j] = issue
          }
        end

        issues.sort! { |a, b|
          state_order.index(a.display_state) <=> state_order.index(b.display_state)
        }.map! { |issue|
          log_issue(issue)
          calc_issue_stats(issue)
        }
      end

      def get_cached_issues
        # NOTE: Use base64 key for cache
        key = Base64.urlsafe_encode64([@options[:group], @options[:group_id]].join)
        group = get_cache 'group', key
      end

      def get_merge_requests(options = {}, log = true)
        options = {
          **scope,
          per_page: 100,
          **options,
        }
        state_order = %w(merged closed draft opened)

        print "Load merge requests ".blue, "(", scope.to_json.italic.light_blue, ") : " if log

        merge_requests = get_issues(options.except(:assignee_id), false).map {|issue|
          gitlab.merge_requests_related_to_issue(issue)
        }.flatten.uniq {|mr| mr.id}

        if assignee
          merge_requests.select! {|mr|
            mr.assignees.any? {|a| a.id == assignee.id} ||
            mr.reviewers.any? {|r| r.id == assignee.id}
          }
        end

        #if project then
        #  merge_requests = gitlab.merge_requests(project.id, options)
        #  merge_requests |= gitlab.merge_requests(project.id,
        #    options.except(:assignee_id).merge(reviewer_id: assignee&.id)
        #  ) if assignee
        #elsif group
        #  merge_requests = gitlab.group_merge_requests(group.id, options)
        #  if assignee
        #    merge_requests += gitlab.group_merge_requests(group.id,
        #      options.except(:assignee_id).merge(reviewer_id: assignee&.id)
        #    )
        #    merge_requests.uniq! {|mr| mr.id}
        #  end
        #else
        #  merge_requests = gitlab.merge_requests(nil, options)
        #  merge_requests |= gitlab.merge_requests(nil,
        #    options.except(:assignee_id).merge(reviewer_id: assignee&.id)
        #  ) if assignee
        #end

        puts " #{merge_requests.count} merge requests".send(merge_requests.count > 0 ? :green : :red) if log

        merge_requests.map { |merge_request|
          decorate_merge_request(merge_request)
        }.sort { |a, b|
          state_order.index(a.display_state) <=> state_order.index(b.display_state)
        }.map { |merge_request|
          log_merge_request(merge_request)
          calc_merge_request_stats(merge_request)
        }
      end

      def get_commits(options = {})
        options = {
          **@options.slice(:ref_name),
          per_page: 100,
          trailers: true,
          **options
        }
        print "Load commits ".blue, "(", options.to_json.italic.light_blue, ") : "
        page = 1
        commits = []
        loop do
          next_commits = gitlab.commits(project.id, **options, page: page )
          break if next_commits.empty?
          print "."
          commits += next_commits
          page += 1
        end
        #commits = gitlab.commits(project.id, **options)
        puts " #{commits.count} commits".send(commits.count > 0 ? :green : :red)
        commits.map { |commit|
          decorate_commit(commit)
        }
      end

      def get_tags
        print "Load tags ".blue, "(", scope.to_json.italic.light_blue, ") : "
        page = 1
        tags = []
        loop do
          next_tags = gitlab.tags(project.id, page: page, per_page: 100)
          break if next_tags.empty?
          print "."
          tags += next_tags
          page += 1
        end
        puts " #{tags.count} tags".send(tags.count > 0 ? :green : :red)
        #tags.each do |tag|
        #  puts "- #{tag.name} #{tag.target} #{tag.commit.id}"
        #end
        tags
      end

      def get_changelog
        changelog = []
        default_tag = {
          name: @options[:ref_name],
          release: nil,
          message: nil,
          features: [],
          hotfixes: [],
          bugfixes: [],
          others: []
        }
        current_tag = deep_clone(default_tag)

        commits.each_with_index do |commit, i|
          #puts "#{commit.author.username}: #{commit.title} (#{commit.merge_request_url})"
          #puts "#{i} #{current_tag[:others].count}"
          if tag = tags.detect { |t| [t.target, t.commit.id].include? commit.id }
            #puts "Change tag #{current_tag[:name]} -> #{tag.name}"
            changelog << current_tag if i > 0
            current_tag = deep_clone({
              **default_tag,
              **tag.to_h,
            })
          elsif i == 0
            current_tag[:commit] = commit.to_h
          end
          if commit.message =~ /branch:? *'?#{@options[:feature_branch]}'?/i ||
             commit.title =~ /^feat *\(?[^\)]*\)? *:/i
            current_tag[:features] << commit.to_h
          elsif commit.message =~ /branch:? *'?#{@options[:hotfix_branch]}'?/i
            current_tag[:hotfixes] << commit.to_h
          elsif commit.message =~ /branch:? *'?#{@options[:bugfix_branch]}'?/i ||
                commit.title =~ /^fix *\(?[^\)]*\)? *:/i
            current_tag[:bugfixes] << commit.to_h
          else
            current_tag[:others] << commit.to_h
          end
        end

        changelog << current_tag if commits.any?

        changelog.map { |tag| to_object(tag) }
      end

      def decorate_issue(issue)
        display_iid = if project then
          issue.references&.short
        elsif group then
          issue.references&.relative
        else
          issue.references&.full
        end
        enhancement = issue.labels.include?(@options[:enhancement_label])
        bug = issue.labels.include?(@options[:bug_label])
        display_state = issue.state == 'closed' ? issue.state
                      : issue.labels.include?(@options[:doing_label]) ? 'doing'
                      : issue.labels.include?(@options[:done_label]) ? 'done'
                      : issue.state

        added_at = Time.parse(issue.created_at)

        if iteration
          events = gitlab.issue_iteration_events(issue.project_id, issue.iid, per_page: 100)
          events.sort { |a, b| a.created_at <=> b.created_at }.each do |event|
            if iteration.title == event.iteration.title
              added_at = Time.parse(event.created_at)
            end
          end
        end

        unplanned = iteration && added_at.to_date > Date.parse(iteration.start_date)

        project = projects.detect {|p| p.id == issue.project_id }
        group_path = issue.references&.full.split('/')[0...-1].join('/')
        group = groups.detect {|g| g.full_path == group_path }
        weight = issue.try(:weight)
        assignee_weight = (weight || 0).to_f / issue.assignees.size

        to_object({
          **issue.to_h,
          assignee_weight:,
          weight:,
          added_at:,
          unplanned:,
          display_iid:,
          display_state:,
          enhancement:,
          bug:,
          project:,
          group:,
        })
      end

      def decorate_merge_request(merge_request)
        display_iid = if project then
          merge_request.references&.short
        elsif group then
          merge_request.references&.relative
        else
          merge_request.references&.full
        end
        display_state = ['closed', 'merged'].include?(merge_request.state) ? merge_request.state
                      : merge_request.draft ? 'draft'
                      : merge_request.state
        to_object({
          **merge_request.to_h,
          display_iid: display_iid,
          display_state: display_state,
        })
      end

      def decorate_commit(commit)
        author = search_user(commit.author_email)
        merge_request_url = commit.message.match(/MR: *([^ ]+)/i).to_a.dig(1)

        to_object({
          **commit.to_h,
          author: author&.to_h,
          merge_request_url: merge_request_url
        })
      end

      def calc_issue_stats(issue)
        total_time_spent = issue&.time_stats&.total_time_spent
        human_total_time_spent = issue&.time_stats&.human_total_time_spent

        if total_time_spent.blank? || total_time_spent == 0
          total_time_spent, doing_time_spent, merge_time_spent = get_issue_time_spent(issue)
          human_total_time_spent = seconds_to_s(total_time_spent)
          human_doing_time_spent = seconds_to_s(doing_time_spent)
          human_merge_time_spent = seconds_to_s(merge_time_spent)
        end

        to_object({
          **issue.to_h,
          time_stats: {
            **issue.time_stats&.to_h,
            total_time_spent: total_time_spent,
            doing_time_spent: doing_time_spent,
            merge_time_spent: merge_time_spent,
            human_total_time_spent: human_total_time_spent,
            human_doing_time_spent: human_doing_time_spent,
            human_merge_time_spent: human_merge_time_spent
          }
        })
      end

      def calc_merge_request_stats(merge_request)
        total_time_spent = merge_request&.time_stats&.total_time_spent
        human_total_time_spent = merge_request&.time_stats&.human_total_time_spent

        if total_time_spent.blank? || total_time_spent == 0
          total_time_spent, draft_time_spent, review_time_spent = get_merge_request_time_spent(merge_request)
          human_total_time_spent = seconds_to_s(total_time_spent)
          human_draft_time_spent = seconds_to_s(draft_time_spent)
          human_review_time_spent = seconds_to_s(review_time_spent)
        end

        to_object({
          **merge_request.to_h,
          time_stats: {
            **merge_request.time_stats&.to_h,
            total_time_spent: total_time_spent,
            draft_time_spent: draft_time_spent,
            review_time_spent: review_time_spent,
            human_total_time_spent: human_total_time_spent,
            human_draft_time_spent: human_draft_time_spent,
            human_review_time_spent: human_review_time_spent
          }
        })
      end

      def log_issue(issue)
        state = issue.display_state.send({
          opened: :white,
          doing: :green,
          done: :yellow,
          closed: :red
        }[issue.display_state&.to_sym] || :light_blue)

        puts "#{issue.display_iid.white.bold} #{issue.title.underline}"
        puts "  state: ".blue + state
        puts "  labels: ".blue + (issue.labels.join(', ') || "-").light_blue
        puts "  milestone: ".blue + (issue.milestone&.title || "-").light_blue
        puts "  iteration: ".blue + (iteration&.title || "-").light_blue
        puts "  assignees: ".blue + (issue.assignees.map { |a| "@#{a['username']} (#{a['name']})" }.join(', ') || "-").light_blue
        puts "  weight: ".blue + (issue.try(:weight)&.to_s || "-").light_blue
        puts "  time spent: ".blue + ("(" + '%.2f' % (issue.time_stats&.total_time_spent/3600) + "h) ") +(issue.time_stats&.human_total_time_spent || "-").light_blue + " ( " +
          "doing: ".blue + (issue.time_stats&.try(:human_doing_time_spent) || "-").light_blue + ', ' +
          "merge: ".blue + (issue.time_stats&.try(:human_merge_time_spent) || "-").light_blue + " )"
      end

      def log_merge_request(merge_request)
        state = merge_request.display_state.send({
          opened: :white,
          doing: :green,
          done: :yellow,
          closed: :red
        }[merge_request.display_state&.to_sym] || :light_blue)

        puts "#{merge_request.display_iid.white.bold} #{merge_request.title.underline}"
        puts "  state: ".blue + state
        puts "  labels: ".blue + (merge_request.labels.join(', ') || "-").light_blue
        puts "  milestone: ".blue + (merge_request.milestone&.title || "-").light_blue
        puts "  iteration: ".blue + (iteration&.title || "-").light_blue
        puts "  assignees: ".blue + (merge_request.assignees.map { |a| "@#{a['username']} (#{a['name']})" }.join(', ') || "-").light_blue
      end

      def get_issue_time_spent(issue)
        started_at = nil
        ended_at = nil
        closed_at = issue.closed_at ? Time.parse(issue.closed_at) : nil
        doing_time_spent = 0
        events = get_issue_label_events(issue.project_id, issue.iid)
        #puts "#{issue.created_at}: Issue created"
        events.sort { |a, b| a.created_at <=> b.created_at }.each do |event|
          if event.label&.name == @options[:doing_label]
            #puts "#{event.created_at}: #{event.action} #{event.label&.name}"
            if event.action == 'add'
              started_at = Time.parse(event.created_at)
              ended_at = nil
            elsif event.action == 'remove' && started_at && (!closed_at || Time.parse(event.created_at) < closed_at)
              ended_at = Time.parse(event.created_at)
              #doing_time_spent += ended_at - started_at
              doing_time_spent += work_seconds_between(started_at, ended_at)
              started_at = nil
            end
          end
        end
        if closed_at
          #puts "#{issue.closed_at}: Issue closed"
          merge_requests = gitlab.merge_requests_related_to_issue(issue)
          mr_created_at = merge_requests.map(&:created_at).min
          mr_time_spents = merge_requests.map {|mr|
            get_merge_request_time_spent(mr)
          }
          mr_total_time_spent = mr_time_spents.map {|a| a[0]}.max
          mr_draft_time_spent = mr_time_spents.map {|a| a[1]}.max
          mr_review_time_spent = mr_time_spents.map {|a| a[2]}.max

          if started_at && !ended_at
            ended_at = closed_at
            doing_time_spent += work_seconds_between(started_at, closed_at)
          end

          started_at ||= mr_created_at && Time.parse(mr_created_at)

          merge_time_spent = work_seconds_between(ended_at || closed_at, closed_at)
          merge_time_spent = [merge_time_spent, mr_review_time_spent || mr_total_time_spent].compact.max
          total_time_spent = work_seconds_between(started_at, closed_at) if started_at
        end
        total_time_spent ||= (doing_time_spent || 0) + (merge_time_spent || 0)
        if (doing_time_spent == 0 || !doing_time_spent) && merge_time_spent && total_time_spent
          doing_time_spent = total_time_spent - merge_time_spent
        end
        return total_time_spent, doing_time_spent, merge_time_spent
      end

      def get_merge_request_time_spent(merge_request)
        opened_at = merge_request.created_at ? Time.parse(merge_request.created_at) : nil
        merged_at = merge_request.merged_at ? Time.parse(merge_request.merged_at) : nil
        closed_at = merge_request.closed_at ? Time.parse(merge_request.closed_at) : nil
        ended_at = merged_at || closed_at
        total_time_spent = ended_at && opened_at ? work_seconds_between(opened_at, ended_at) : 0
        draft_time_spent = 0
        review_time_spent = 0

        #result = gitlab.merge_request_state_events(merge_request.project_id, merge_request.iid)
        notes = gitlab.merge_request_notes(merge_request.project_id, merge_request.iid, per_page: 100)

        draft_started_at = opened_at
        draft_ended_at = nil
        review_started_at = nil
        review_ended_at = nil

        notes.sort { |a, b| a.created_at <=> b.created_at }.each do |note|
          note_created_at = Time.parse(note.created_at)
          #puts "#{note.created_at}: #{(note.body.length > 50 ? "#{note.body[0...50]}..." : note.body).split("\n")[0]}"
          break if ended_at && note_created_at > ended_at
          if note.body == "marked this merge request as **draft**"
            draft_started_at = note_created_at
            draft_ended_at = nil
          elsif note.body == "marked this merge request as **ready**"
            if draft_ended_at
              draft_time_spent += work_seconds_between(draft_ended_at, note_created_at)
              draft_ended_at = nil
            elsif draft_started_at
              draft_time_spent += work_seconds_between(draft_started_at, note_created_at)
              draft_started_at = nil
            end
            draft_ended_at = note_created_at
            draf_started_at = nil
          elsif note.body.start_with? "requested review from"
            if draft_ended_at
              draft_time_spent += work_seconds_between(draft_ended_at, note_created_at)
              draft_ended_at = nil
            elsif draft_started_at
              draft_time_spent += work_seconds_between(draft_started_at, note_created_at)
              draft_started_at = nil
            end
            review_started_at = note_created_at
            review_ended_at = nil
          elsif ["approved this merge request", "resolved all threads"].include? note.body
            if review_ended_at
              review_time_spent += work_seconds_between(review_ended_at, note_created_at)
              review_ended_at = nil
            elsif review_started_at
              review_time_spent += work_seconds_between(review_started_at, note_created_at)
              review_started_at = nil
            end
            review_ended_at = note_created_at
          end
        end
        #result = gitlab.merge_request_changes(merge_request.project_id, merge_request.iid)
        #p "notes: ", result.user_notes_count
        #p "changes: ", result.changes_count
        #p result.changes.inject([0,0]) {|sum, change|
        #  change.diff.split("\n").inject(sum) {|s, c|
        #    adds, dels = s
        #    /^@@.*\-\d+\,(?<del>[^\s]+).*\+\d+\,(?<add>[^\s]+).*@@/ =~ c
        #    puts "@@ -#{del} +#{add} @@"
        #    diff = add.to_i - del.to_i
        #    diff > 0 ? [adds+diff, dels] : [adds, dels-diff]
        #  }
        #}
        #p "======================"

        # FIXME: Actual review time
        #review_time_spent = total_time_spent - draft_time_spent

        return total_time_spent, draft_time_spent, review_time_spent
      end

      def get_issue_label_events(project, issue_iid)
        gitlab.issue_label_events(project, issue_iid)
      end

      def get_epic(epic_iid)
        gitlab.epic(group.id, epic_iid)
      end

      def edit_epic(epic_iid, options = {})
        gitlab.edit_epic(group.id, epic_iid, options)
      end

      def create_epic(options = {})
        title = options.delete('title')
        gitlab.create_epic(group.id, title, options)
      end

      def create_epic_note(epic_id, note)
        gitlab.create_epic_note(group.id, epic_id, note)
      end

      def edit_epic(epic_iid, options = {})
        gitlab.edit_epic(group.id, epic_iid, options)
      end

      def delete_epic(epic_iid)
        gitlab.delete_epic(group.id, epic_iid)
      end

      def epic_issues(epic_iid, options = {})
        gitlab.epic_issues(group.id, epic_iid, options)
      end

      def edit_issue(project_id, id, options = {})
        gitlab.edit_issue(project_id, id, options)
      end

      private

      def sanitize_filename(filename)
        filename
          .split(/(?<=.)\.(?=[^.])(?!.*\.[^.])/m)
          .map { |s| s.gsub /[^a-z0-9\-]+/i, '_' }
          .join('.')
      end

      def get_cache(key, param_key, param_val)
        cache_file = File.join(
          '../../../cache',
          key,
          params_key,
          sanitize_filename("#{param_val}.json")
        )
        return nil unless File.file?(cache_file)
        File
      end

      def set_cache(key, params_key, params_val, object)
        cache_file = File.join(
          '../../../cache',
          key,
          params_key,
          sanitize_filename("#{param_val}.json")
        )
        File.write(cache_file, object.to_hash.to_json)
      end

      def get_user
        gitlab.user
      end

      def get_group
        if @options[:group].present? || @options[:group_id].present?
          print "Load group ".blue
          if @options[:group_id].present?
            print "by group_id='", @options[:group_id].light_blue, "' : "
            group = gitlab.group(@options[:group_id])
          elsif @options[:group].present?
            print "by name ", @options[:group].light_blue, ": "
            groups = gitlab.group_search(@options[:group], per_page: 10)
            group = groups.detect {|g| g.full_path == @options[:group].downcase }
            group ||= groups.detect {|g| g.path == @options[:group].downcase }
            group ||= groups.detect {|g| g.name.downcase.include? @options[:group].downcase }
            group ||= groups.first
          end
          puts group ? "#{group.name} (ID: #{group.id})".green : "-".red
        end
        #set_cache 'group', @options.slice(:group, :group_id)
        group
      end

      def get_cached_group
        # NOTE: Use base64 key for cache
        get_cache 'group', @options.slice(:group, :group_id)
      end

      def get_groups
        if @options[:group].present? || @options[:group_id].present?
          print "Load groups ".blue
          if @options[:group_id].present?
            print "by group_id='", @options[:group_id].light_blue, "' : "
            groups = gitlab.group_subgroups(@options[:group_id], per_page: 100)
          elsif @options[:group]
            print "by group='", @options[:group].light_blue, "' : "
            groups = gitlab.group_subgroups(group.id, per_page: 100)
          else
            groups = gitlab.groups(per_page: 100)
          end
        end
        groups
      end

      def get_project
        if @options[:project].present? || @options[:project_id].present?
          print "Load project ".blue
          if @options[:project_id].present?
            print "by project_id='", @options[:project_id].light_blue, "' : "
            project = gitlab.project(@options[:project_id])
          elsif @options[:project].present?
            print "by name='", @options[:project].light_blue, "' : "
            project = gitlab.project_search(@options[:project], per_page: 1)&.first
          end
          puts project ? "#{project.name} (ID: #{project.id})".green : "-".red
        end
        project
      end

      def get_projects
        if @options[:group].present? || @options[:group_id].present?
          print "Load projects ".blue
          if @options[:group_id].present?
            print "by group_id='", @options[:group_id].light_blue, "' : "
            projects = gitlab.group_projects(@options[:group_id], per_page: 100, include_subgroups: true)
          elsif @options[:group]
            print "by group='", @options[:group].light_blue, "' : "
            projects = gitlab.group_projects(group.id, per_page: 100, include_subgroups: true)
          else
            projects = gitlab.projects(per_page: 100)
          end
        end
        projects
      end

      def get_assignee
        if @options[:assignee].present? || @options[:assignee_id].present?
          print "Load assignee ".blue
          if @options[:assignee_id].present?
            print "by assignee_id='", @options[:assignee_id].light_blue, "' : "
            assignee = gitlab.user(@options[:assignee_id])
          elsif @options[:assignee].present?
            print "by name='", @options[:assignee].light_blue, "': "
            assignee = gitlab.user_search(@options[:assignee], per_page: 1)&.first
          end
          puts assignee ? "#{assignee.name || '-'} (ID: #{assignee.id || '-'})".green : "-".red
        end
        assignee
      end

      def get_milestone
        if @options[:milestone].present?
          if group.present?
            print "Load group milestone ".blue, "'", @options[:milestone].light_blue, "' : "
            milestones = gitlab.group_milestones(group.id, search: @options[:milestone])
          elsif project.present?
            print "Load project milestone ".blue, "'", @options[:milestone].light_blue, "' : "
            milestones = gitlab.milestones(project.id, search: @options[:milestone])
          end
          milestone = milestones.detect {|g| g.title.downcase.include? @options[:milestone].downcase }
          milestone ||= milestones.first
          puts milestone ? "#{milestone.title} (ID: #{milestone.id})".green : "-".red
        end
        milestone
      end

      def get_iteration
        if @options[:iteration].present?
          if group.present?
            print "Load group iteration ".blue, "'", @options[:iteration].light_blue, "' : "
            iterations = gitlab.group_iterations(group.id, search: @options[:iteration])
          elsif project.present?
            print "Load project iteration".blue, "'", @options[:iteration].light_blue, "' : "
            iterations = gitlab.iterations(project.id, search: @options[:iteration])
          end
          iteration = iterations.detect {|g| g.title.downcase.include? @options[:iteration].downcase }
          iteration ||= iterations.first
          puts iteration ? "#{iteration.title} (ID: #{iteration.id})".green : "-".red
        end
        iteration
      end

      def get_iterations
        if @options[:iteration_at].present?
          print "Load iterations ".blue, "(", scope.to_json.italic.light_blue, ") : "
          page = 1
          iterations = []
          loop do
            if group.present?
              next_iterations = gitlab.group_iterations(group.id, page:, per_page: 100)
            elsif project.present?
              next_iterations = gitlab.iterations(project.id, page:, per_page: 100)
            end
            break if next_iterations.empty?
            print "."
            iterations += next_iterations
            page += 1
          end
          puts " #{iterations.count} iterations".send(iterations.count > 0 ? :green : :red)

          iterations = iterations.select {|g|
            DateTime.parse(g.start_date) <= @options[:iteration_at] && DateTime.parse(g.due_date) >= @options[:iteration_at]
          }
        end
        iterations
      end

      def get_members
        if @options[:group].present? || @options[:group_id].present?
          print "Load members ".blue
          if @options[:group_id].present?
            print "by group_id='", @options[:group_id].light_blue, "' : "
            members = gitlab.group_members(@options[:group_id], per_page: 100, include_subgroups: true)
          elsif @options[:group]
            print "by group='", @options[:group].light_blue, "' : "
            members = gitlab.group_members(group.id, per_page: 100, include_subgroups: true)
          else
            members = gitlab.users(per_page: 100)
          end
        end
        members
      end

      def search_user(search)
        @users ||= []
        @user_searches ||= []
        if search.present?
          unless user = @users.detect { |u| [u.email, u.name, u.username].include? search }
            return if @user_searches.include?(search)
            print "Load user ".blue, search.light_blue, "' : "
            user = gitlab.user_search(search, per_page: 1)&.first
            puts user ? "#{user.name || '-'} (ID: #{user.id || '-'})".green : "-".red
            if user
              email = search =~ URI::MailTo::EMAIL_REGEXP ? search : nil
              user = to_object({ **user.to_h, email: email })
              @users << user
            end
          end
          @user_searches << search unless @user_searches.include?(search)
          user
        end
      end

    end
  end
end
