require 'gitlab/tools/helpers'
require 'gitlab/tools/renderer'
require 'gitlab/tools/api'
require 'csv'
require 'wicked_pdf'
require 'tilt'

Encoding.default_external = Encoding::UTF_8

module Gitlab
  module Tools
    class ReportBuilder
      ENCODING = 'UTF-8'
      FORMATS = %w(pdf)

      include Helpers
      include Renderer

      def initialize(options)
        @options = {
          output: 'report.csv',#.pdf',
          format: 'csv',#'pdf',
          doing_label: 'state::doing',
          title: 'Gitlab Report',
          description: 'Issues, merge requests & time statistics',
          date: Time.now.strftime("%d/%m/%Y %H:%M:%S"),
          updated_before: nil,
          updated_after: nil,
          iterations_at: nil,
          detail: true
        }.merge(options)

        @api = Api.new(@options)
      end

      def generate
        puts "Generating report (".blue + @options[:format].light_blue + "): ".blue + @options[:output].green
        set_data
        public_send("generate_#{@options[:format]}")
        puts "Successfully generated ".blue + @options[:output].green
      end

      def generate_pdf
        html = render 'pdf/report.html.erb', layout: 'pdf/layout.html.erb'
        #File.open('report.html', 'wb') { |f| f << html }
        pdf = WickedPdf.new.pdf_from_string(html, pdf_options)
        File.open(@options[:output], 'wb') { |f| f << pdf }
      end

      def generate_csv
        headers = ['Assignee',
                   'Closed Issues', 'Weight', 'Individual',
                   'Bug', 'Enhancement', 'OtherType',
                   'Asio', 'Legacy', 'OtherProject',
                   'Back', 'Front', 'Devops', 'UX (count)', 'OtherLabel']
        CSV.open(@options[:output], 'w') do |csv|
          csv << headers
          @api.members.each do |user|
            issues = @api.issues.select {|i| i.assignees.any? {|a| a.id == user.id }}
            closed_issues = issues.select {|i| i.display_state == 'closed' }
            assignee = user.username
            weight = closed_issues.map(&:weight).compact.inject(0, :+)
            assignee_weight = closed_issues.map(&:assignee_weight).compact.inject(0, :+)
            bug_weight = closed_issues.select(&:bug).map(&:assignee_weight).compact.inject(0, :+)
            enhancement_weight = closed_issues.select(&:enhancement).map(&:assignee_weight).compact.inject(0, :+)
            other_type_weight = closed_issues.select {|i| !i.bug && !i.enhancement }.map(&:assignee_weight).compact.inject(0, :+)
            asio_weight = closed_issues.select {|i| i.group&.path == 'asio' }.map(&:assignee_weight).compact.inject(0, :+)
            legacy_weight = closed_issues.select {|i| i.group&.path == 'legacy' }.map(&:assignee_weight).compact.inject(0, :+)
            other_project_weight = closed_issues.select {|i| i.group&.path !~ /^(asio|legacy)/ }.map(&:assignee_weight).compact.inject(0, :+)

            #api_weight = closed_issues.select {|i| i.labels.include?('api') }.map(&:assignee_weight).compact.inject(0, :+)
            #web_weight = closed_issues.select {|i| i.labels.include?('web') }.map(&:assignee_weight).compact.inject(0, :+)
            #devops_weight = closed_issues.select {|i| (['ci', 'devops', 'infra'] & i.labels).any? }.map(&:assignee_weight).compact.inject(0, :+)
            #ux_count = closed_issues.select {|i| i.labels.include?('ux / ui') }.size
            #other_label_weight = closed_issues.select {|i| (['api', 'web', 'ci', 'devops', 'infra', 'ux / ui'] & i.labels).empty? }.map(&:assignee_weight).compact.inject(0, :+)

            api_weight = closed_issues.map do |issue|
              next unless issue.labels.include?('api')
              divisor = 1
              divisor += 1 if issue.labels.include?('web')
              divisor += 1 if issue.labels.include?('ux / ui')
              divisor += 1 if (['ci', 'devops', 'infra'] & issue.labels).any?
              issue.assignee_weight / divisor.to_f
            end.compact.inject(0, :+)

            web_weight = closed_issues.map do |issue|
              next unless issue.labels.include?('web')
              divisor = 1
              divisor += 1 if issue.labels.include?('api')
              divisor += 1 if issue.labels.include?('ux / ui')
              divisor += 1 if (['ci', 'devops', 'infra'] & issue.labels).any?
              issue.assignee_weight / divisor.to_f
            end.compact.inject(0, :+)

            devops_weight = closed_issues.map do |issue|
              next unless (['ci', 'devops', 'infra'] & issue.labels).any?
              divisor = 1
              divisor += 1 if issue.labels.include?('api')
              divisor += 1 if issue.labels.include?('web')
              divisor += 1 if issue.labels.include?('ux / ui')
              issue.assignee_weight / divisor.to_f
            end.compact.inject(0, :+)

            ux_count = closed_issues.map do |issue|
              next unless issue.labels.include?('ux / ui')
              divisor = 1
              divisor += 1 if issue.labels.include?('api')
              divisor += 1 if issue.labels.include?('web')
              divisor += 1 if (['ci', 'devops', 'infra'] & issue.labels).any?
              1 / divisor.to_f
            end.compact.inject(0, :+)

            other_label_weight = closed_issues.map do |issue|
              next if (['ci', 'devops', 'infra'] & issue.labels).any? ||
                      issue.labels.include?('api') ||
                      issue.labels.include?('web') ||
                      issue.labels.include?('ux / ui')
              issue.assignee_weight
            end.compact.inject(0, :+)

            csv << [assignee, closed_issues.count, weight, assignee_weight,
                    bug_weight, enhancement_weight, other_type_weight,
                    asio_weight, legacy_weight, other_project_weight,
                    api_weight, web_weight, devops_weight, ux_count, other_label_weight]
          end
        end
      end

      def pdf_options
        {
          encoding: 'utf8',
          page_size: 'A4',
          viewport_size: '1280x1024',
          show_as_html: true,
          disable_internal_links: false,
          disable_external_links: false,
          keep_relative_links: false,
          disable_local_file_access: false,
          enable_local_file_access: true,
          log_level: 'info',
          footer: {
            right: '[date] [time] - page [page] of [topage]',
            font_size: '6'
          },
          margin: {
            top: 5,
            bottom: 5,
            left: 5,
            right: 5
          }
        }
      end

      private

      def set_data
        @issues = @api.issues
        @iteration = @api.iteration
      end
    end
  end
end
