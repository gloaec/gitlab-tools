# Gitlab::Tools

## Installation

Bundle & Install :

```bash
$ bundle install
$ bundle exec rake install
```

Method 2 :

```bash
$ gem build gitlab-tools.gemspec
$ gem install ./gitlab-tools-0.1.0.gem
```

## Usage

```
Usage: gitlab-report COMMAND [OPTIONS]"

Commands
    generate: Generates report for given scope"
    changelog: Generates changelog for given scope"
    timestamp: Get/Set time spent on issues"
    move: Moves issue/epic to epic"
```

### `gitlab-tools generate`

Options :

- `--token <token>`: Gitlab token
- `--format (pdf|csv)`: Output format
- `--iteration-at <date>`: Filter by iteration date
- `--group <group_name>`: Filter by group name
- `--group-id <group_id>`: Filter by group id
- `--project <group_name>`: Filter by project name
- `--project-id <group_id>`: Filter by project id
- `--assignee <assignee_name>`: Filter by assignee name
- `--assignee-id <assignee_id>`: Filter by assignee id
- `--milestone <milestone>`: Filter by milestone id (deprecated)
- `--iteration <iteration>`: Filter by iteration id (deprecated)

### `gitlab-tools changelog`

TODO

### `gitlab-tools timestamp`

TODO

### `gitlab-tools move`

TODO

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/gitlab-tools.

## TODO

- Use pagination: 100 limit for resources or lazy-paginate

## Ideas

- [ ] Per assignee stats
  - [ ] Appovals
  - [ ] Issues commented
- [x] Changelog generator

